# file: wordCount.py   upokcxgpofgcx

import sys

# Make a translation table for getting rid of non-word characters.
# (Copied from lecture notes.)
dropChars = "!@#$%^&*()_+-={}[]|\\:;\"'<>,.?/1234567890"
dropDict = dict([(c, ' ') for c in dropChars])
dropTable = str.maketrans(dropDict)

# Read a file and build the table.
table = {} # dictionary: words -> frequencies
for line in sys.stdin:
   words = line.upper().translate(dropTable).split()
   for word in words:
      if word in table:
         table[word] += 1
      else:
         table[word] = 1

# extract and sort
wfs = sorted([(f,w) for (w, f) in table.items()]) # simpler way?
wfs.reverse()

# Print the table 
for (f,w) in wfs: # avoid pattern bindings?
   print(w, f)
